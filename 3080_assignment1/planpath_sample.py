import argparse as ap
import re
import platform

from dls_search import DLS
from a_search import ASearch


######## RUNNING THE CODE ####################################################
#   You can run this code from terminal by executing the following command
#   python planpath.py <INPUT/INPUT#.txt> <OUTPUT/OUTPUT#.txt> <flag> <algorithm>
#   for example: python planpath.py INPUT/input2.txt OUTPUT/output2.txt 0 A
#   NOTE: THIS IS JUST ONE EXAMPLE INPUT DATA
###############################################################################

################## YOUR CODE GOES HERE ########################################


def graphsearch(map, dimension, flag, procedure_name):
    solution = ""
    path = None
    cost = None
    if procedure_name == "D":
        bound = 10  # you have to determine its value
        dls = DLS(map, dimension, depth=bound, diagnostic=(flag >= 1))
        path, cost = dls.find_solution()
        print("The path that can reach the goal is: " + str(path))
        print("Total cost of this path is: " + str(cost))

    elif procedure_name == "A":
        a_search = ASearch(map, dimension, diagnostic=(flag >= 1))
        path, cost = a_search.find_solution()
        print("The path that can reach the goal is: " + str(path))
        print("Total cost of this path is: " + str(cost))
    else:
        print("invalid procedure name")

    solution = "The path that can reach the goal is: " + str(path) + "\n" + \
               "Total cost of this path is: " + str(cost) + "\n"
    return solution


def read_from_file(file_name):
    """
    parse the INPUT file
    :param file_name:
    :return: dimension: the dimension of the INPUT map; question_map: an 2D array representative of the map
    """
    # You can change the file reading function to suit the way
    # you want to parse the file
    file_handle = open(file_name)
    dimension = int(file_handle.readline())
    question_map = [["" for _ in range(dimension)] for _ in range(dimension)]
    for i in range(dimension):
        row = file_handle.readline()
        for j in range(dimension):
            question_map[i][j] = row[j]

    return question_map, dimension


###############################################################################
########### DO NOT CHANGE ANYTHING BELOW ######################################
###############################################################################

def write_to_file(file_name, solution):
    file_handle = open(file_name, 'w')
    file_handle.write(solution)

def main():
    # create a parser object
    parser = ap.ArgumentParser()

    # specify what arguments will be coming from the terminal/commandline
    parser.add_argument("input_file_name", help="specifies the name of the INPUT file", type=str)
    parser.add_argument("output_file_name", help="specifies the name of the OUTPUT file", type=str)
    parser.add_argument("flag", help="specifies the number of steps that should be printed", type=int)
    parser.add_argument("procedure_name", help="specifies the type of algorithm to be applied, can be D, A", type=str)

    # get all the arguments
    arguments = parser.parse_args()

##############################################################################
# these print statements are here to check if the arguments are correct.
#    print("The input_file_name is " + arguments.input_file_name)
#    print("The output_file_name is " + arguments.output_file_name)
#    print("The flag is " + str(arguments.flag))
#    print("The procedure_name is " + arguments.procedure_name)
##############################################################################

    # Extract the required arguments

    operating_system = platform.system()

    if operating_system == "Windows":
        input_file_name = arguments.input_file_name
        input_tokens = input_file_name.split("\\")
        if not re.match(r"(INPUT\\INPUT)(\d)(.txt)", input_file_name):
            print("Error: INPUT path should be of the format INPUT\INPUT#.txt")
            return -1

        output_file_name = arguments.output_file_name
        output_tokens = output_file_name.split("\\")
        if not re.match(r"(OUTPUT\\OUTPUT)(\d)(.txt)", output_file_name):
            print("Error: OUTPUT path should be of the format OUTPUT\OUTPUT#.txt")
            return -1
    else:
        input_file_name = arguments.input_file_name
        input_tokens = input_file_name.split("/")
        if not re.match(r"(INPUT/INPUT)(\d)(.txt)", input_file_name):
            print("Error: INPUT path should be of the format INPUT/INPUT#.txt")
            return -1

        output_file_name = arguments.output_file_name
        output_tokens = output_file_name.split("/")
        if not re.match(r"(OUTPUT/OUTPUT)(\d)(.txt)", output_file_name):
            print("Error: OUTPUT path should be of the format OUTPUT/OUTPUT#.txt")
            return -1

    flag = arguments.flag
    procedure_name = arguments.procedure_name

    try:
        map, dimension = read_from_file(input_file_name)  # get the map
    except FileNotFoundError:
        print("INPUT file is not present")
        return -1

    print("Map dimension: " + str(dimension))
    print("Map: ")
    for r in map:
        print(r)
    print("-------------------------------\n")

    solution_string = "" # contains solution
    write_flag = 0 # to control access to OUTPUT file

    # take a decision based upon the procedure name
    if procedure_name == "D" or procedure_name == "A":
        solution_string = graphsearch(map, dimension, flag, procedure_name)
        write_flag = 1
    else:
        print("invalid procedure name")

    # TODO: write the result to a file
    # call function write to file only in case we have a solution
    if write_flag == 1:
        write_to_file(output_file_name, solution_string)


if __name__ == "__main__":
    # main()
    from search_helper import Node
    list = []
    n1 = Node(identifier=-1)
    n2 = Node(identifier=5, i=1)
    list.append(n1)
    print(n2 in list)
    k = list.index(n2)
    print(list[k])