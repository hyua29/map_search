import bisect
from search_helper import Node
from search_helper import Search
import search_helper


class DLS(Search):

    def find_solution(self):
        """ find the starting point and the end point """
        s_i = -1
        s_j = -1
        for i in range(self.dimension):
            for j in range(self.dimension):
                if self.map[i][j] == search_helper.START_POINT:
                    s_i = i
                    s_j = j
                if self.map[i][j] == search_helper.END_POINT:
                    self.goal_node_position = (i, j)

        """ create the starting point of the map """
        explored_position = []
        start_node = Node(self._get_id_counter(), i=s_i, j=s_j, g=0, h=0, path_to_node=["S"], step_to_node="S",
                          expansion_order=self._get_expansion_order())
        self.open = [start_node]
        self.close = []
        result_path, total_cost = self._find_solution_rec(self.map, start_node, explored_position)
        return result_path, total_cost

    def _find_solution_rec(self, current_map, current_node, explored_position):

        """ remove from the open and put it in close """
        self.close.append(current_node)
        self.open.remove(current_node)
        # TODO: replace this with close
        explored_position.append((current_node.i, current_node.j))

        """ return the result if the goal has been found """
        if current_node.is_goal:
            """ print diagnostic message """
            if self.diagnostic >= current_node.expansion_order:
                self._print_state(current_node)
            return current_node.path_to_node, current_node.f

        """ the algorithm has max depth and its has reached max depth """
        if not self.max_depth == -1 and len(current_node.path_to_node) > self.max_depth:
            print("Exceeded the max depth")
            return [], -1

        """ expand the node, straight moves """
        for key, direction in search_helper.straight_directions.items():
            if self._not_out_of_bounds(current_node, direction):
                new_node = current_node.move(direction, search_helper.COST_NORMAL_MOVE, new_h=0)

                if not (self.open.__contains__(new_node) or self.close.__contains__(
                        new_node)):  # if this node has never been found
                    new_node.set_identifier(self._get_id_counter())
                    bisect.insort_left(self.open, new_node)
                elif self.open.__contains__(new_node):
                    index = self.open.index(new_node)
                    new_node = self.open[index]
                elif self.close.__contains__(new_node):
                    index = self.close.index(new_node)
                    new_node = self.close[index]

                current_node.add_child_id(new_node.identifier)

        """ diagonal moves """
        for key, direction in search_helper.diagonal_directions.items():
            if self._not_out_of_bounds(current_node, direction):
                new_node = current_node.move(direction, search_helper.COST_DIAGONAL_MOVE, new_h=0)

                if not (self.open.__contains__(new_node) or self.close.__contains__(
                        new_node)):  # if this node has never been found
                    new_node.set_identifier(self._get_id_counter())
                    bisect.insort_left(self.open, new_node)
                elif self.open.__contains__(new_node):
                    index = self.open.index(new_node)
                    new_node = self.open[index]
                elif self.close.__contains__(new_node):
                    index = self.close.index(new_node)
                    new_node = self.close[index]

                current_node.add_child_id(new_node.identifier)

        """ print diagnostic message """
        if self.diagnostic >= current_node.expansion_order:
            self._print_state(current_node)

        for id in current_node.child:
            child_node = self._get_node_by_id(id)
            """ skip the node ig it's been explored through another path """
            if self._node_is_in_close(id):
                continue
            """ assign expansion order; for DLS, this is acquired the actual order the of expansion """
            child_node.expansion_order = self._get_expansion_order()

            """ expand the child node """
            modified_map = self.make_a_move(current_map, current_node, child_node)  # move towards the target up by one
            path_next_node, cost_next_node = self._find_solution_rec(modified_map, child_node, explored_position)
            """ found the solution"""
            if not cost_next_node == -1:
                return path_next_node, cost_next_node

        """ All paths have reached dead end """
        return [], -1


def read_from_file(file_name):
    """
    parse the INPUT file
    :param file_name:
    :return: dimension: the dimension of the INPUT map; question_map: an 2D array representative of the map
    """
    # You can change the file reading function to suit the way
    # you want to parse the file
    file_handle = open(file_name)
    dimension = int(file_handle.readline())
    print(dimension)
    question_map = [["" for _ in range(dimension)] for _ in range(dimension)]
    for i in range(dimension):
        row = file_handle.readline()
        for j in range(dimension):
            question_map[i][j] = row[j]

    return question_map, dimension


start_map, map_d = read_from_file("sample_input.txt")
a_search = DLS(start_map, map_d, diagnostic=1000)
path, cost = a_search.find_solution()

print("The path that can reach the goal is: " + str(path))
print("Total cost of this path is: " + str(cost))
