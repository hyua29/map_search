import copy

COST_DIAGONAL_MOVE = 1
COST_NORMAL_MOVE = 2

NORMAL_PATH = "M"
MOUNTAIN = "X"
START_POINT = "S"
END_POINT = "G"

DIRECTION_UP = "U"
DIRECTION_DOWN = "D"
DIRECTION_LEFT = "L"
DIRECTION_RIGHT = "R"
DIRECTION_UP_LEFT = "UL"
DIRECTION_UP_RIGHT = "UR"
DIRECTION_DOWN_LEFT = "DL"
DIRECTION_DOWN_RIGHT = "DR"
straight_directions = {"DIRECTION_UP": "U", "DIRECTION_DOWN": "D", "DIRECTION_LEFT": "L", "DIRECTION_RIGHT": "R"}
diagonal_directions = {"DIRECTION_UP_LEFT": "UL", "DIRECTION_UP_RIGHT": "UR", "DIRECTION_DOWN_LEFT": "DL", "DIRECTION_DOWN_RIGHT": "DR"}


class Node:
    def __init__(self, identifier, i=-1, j=-1, g=0, h=0,
                 path_to_node=None, step_to_node=None, is_goal=False, expansion_order=-1):
        self.identifier = "N" + str(identifier)
        self.i = i
        self.j = j
        self.g = g
        self.h = h
        self.f = g + h
        self.path_to_node = path_to_node
        self.step_to_node = step_to_node
        self.is_goal = is_goal
        self.expansion_order = expansion_order
        self.child = []

    def add_child_id(self, identifier):
        """
        add a pointer to a new child
        :param identifier: identifier of the child node
        :return:
        """
        self.child.append(identifier)

    def set_identifier(self, id):
        self.identifier = "N" + str(id)

    def move(self, direction, action_cost, new_identifier=-1, new_h=0):
        """
        make a move
        :param new_identifier:
        :param direction: direction to move on
        :param action_cost: the cost of the move
        :param new_h: new heuristic cost of this node
        :return: new node
        """
        """ make a copy of the current path and append the action to reach the new node """
        path_copy = copy.deepcopy(self.path_to_node)
        path_copy.append(direction)
        if direction == DIRECTION_UP:
            return Node(new_identifier, i=self.i - 1, j=self.j, g=self.g + action_cost,
                        h=new_h,
                        path_to_node=path_copy, step_to_node=direction)
        elif direction == DIRECTION_DOWN:
            return Node(new_identifier, i=self.i + 1, j=self.j, g=self.g + action_cost,
                        h=new_h,
                        path_to_node=path_copy, step_to_node=direction)
        elif direction == DIRECTION_LEFT:
            return Node(new_identifier, i=self.i, j=self.j - 1, g=self.g + action_cost,
                        h=new_h,
                        path_to_node=path_copy, step_to_node=direction)
        elif direction == DIRECTION_RIGHT:
            return Node(new_identifier, i=self.i, j=self.j + 1, g=self.g + action_cost,
                        h=new_h,
                        path_to_node=path_copy, step_to_node=direction)
        elif direction == DIRECTION_UP_LEFT:
            return Node(new_identifier, i=self.i - 1, j=self.j - 1,
                        g=self.g + action_cost, h=new_h,
                        path_to_node=path_copy, step_to_node=direction)
        elif direction == DIRECTION_UP_RIGHT:
            return Node(new_identifier, i=self.i - 1, j=self.j + 1,
                        g=self.g + action_cost, h=new_h,
                        path_to_node=path_copy, step_to_node=direction)
        elif direction == DIRECTION_DOWN_LEFT:
            return Node(new_identifier, i=self.i + 1, j=self.j - 1,
                        g=self.g + action_cost, h=new_h,
                        path_to_node=path_copy, step_to_node=direction)
        elif direction == DIRECTION_DOWN_RIGHT:
            return Node(new_identifier, i=self.i + 1, j=self.j + 1,
                        g=self.g + action_cost, h=new_h,
                        path_to_node=path_copy, step_to_node=direction)

    def update_h(self, new_h):
        self.h = new_h
        self.f = self.g + new_h

    def __eq__(self, other):
        return (self.identifier == other.identifier) or\
               (self.i == other.i and self.j == other.j)

    def __lt__(self, other):
        return self.f < other.f

    def __gt__(self, other):
        return self.f > other.f

    def __ne__(self, other):
        return not ((self.identifier == other.identifier) or
               (self.i == other.i and self.j == other.j))

    def __le__(self, other):
        return self.f <= other.f

    def __ge__(self, other):
        return self.f >= other.f

    def __str__(self):
        return "{identifier}:{path_to_node} {expansion_order} {g} {h} {f}".format(
            identifier=self.identifier, path_to_node=self.path_to_node, expansion_order=self.expansion_order, g=self.g,
            h=self.h, f=self.f)


class Search:
    def __init__(self, map, dimension, depth=-1, diagnostic=False):
        """
        Search algorithm
        :param map: the map to run through
        :param dimension: dimension of the map
        :param depth: max depth of the algorithm, -1 if no depth limit
        :param diagnostic:
        """
        self.map = map
        self.dimension = dimension
        self.open = []
        self.close = []
        self.max_depth = depth
        self.expansion_order = 1
        self.node_id_counter = 0
        self.diagnostic = diagnostic
        self.goal_node_position = None

    def _get_expansion_order(self):
        """
        get the expansion_order and increased it by one
        :return:
        """
        e = self.expansion_order
        self.expansion_order += 1
        return e

    def _get_id_counter(self):
        """
        get the node_id_counter and increased it by one
        :return:
        """
        i = self.node_id_counter
        self.node_id_counter += 1
        return i

    def _get_node_by_id(self, node_id):
        """
        This function returns the node the node exists
        :param node_id:
        :return:
        """
        for n in self.open:
            if n.identifier == node_id:
                return n
        for n in self.close:
            if n.identifier == node_id:
                return n
        return None

    def _node_is_in_close(self, node_id):
        for n in self.close:
            if n.identifier == node_id:
                return True
        return False

    def _node_is_in_open(self, node_id):
        for n in self.open:
            if n.identifier == node_id:
                return True
        return False

    def _get_node_by_position(self, i, j):
        for n in self.open:
            if n.i == i and n.j ==j:
                return n
        for n in self.close:
            if n.i == i and n.j ==j:
                return n
        return None

    def _print_state(self, current_node):
        result = str(current_node) + "\n"
        children_message = "CHILDREN: { "
        for id in current_node.child:
            dummy_node = Node(identifier=-1)
            dummy_node .identifier = id
            if dummy_node in self.open:
                child_index = self.open.index(dummy_node)
                children_message += "(" + str(self.open[child_index]) + ")， "
            elif dummy_node in self.close:
                child_index = self.close.index(dummy_node)
                children_message += "(" + str(self.close[child_index]) + ")， "
            else:
                raise ValueError("Child node is supposed to be in either open or close list, but it cannot be found")

        children_message = children_message[:-2]
        children_message += " } \n"
        result += children_message

        open_message = "OPEN: { "
        for o in self.open:
            open_message += "(" + str(o) + ")， "
        open_message = open_message[:-2]
        open_message += " } \n"
        result += open_message

        close_message = "CLOSE: { "
        for cl in self.close:
            close_message += "(" + str(cl) + ")， "
        close_message = close_message[:-2]
        close_message += " } \n"
        result += close_message

        print(result)
        return result

    def make_a_move(self, current_map, current_node, target_node):
        """
        make a move
        swap the symbol of current location with the target location
        :param current_map:
        :param current_node: from which node the target node is reached
        :param target_node: the node to move to
        :return True if the target location is the goal location otherwise False
        """
        new_map = copy.deepcopy(current_map)

        if new_map[target_node.i][target_node.j] == END_POINT:
            target_node.is_goal = True
        current_char = new_map[current_node.i][current_node.j]
        new_map[current_node.i][current_node.j] = new_map[target_node.i][target_node.j]
        new_map[target_node.i][target_node.j] = current_char

        return new_map

    def _not_out_of_bounds(self, node, direction):
        if direction == DIRECTION_UP:
            return node.i - 1 >= 0 \
                   and not self.map[node.i - 1][node.j] == MOUNTAIN

        elif direction == DIRECTION_DOWN:
            return node.i + 1 < self.dimension \
                   and not self.map[node.i + 1][node.j] == MOUNTAIN

        elif direction == DIRECTION_LEFT:
            return node.j - 1 >= 0 \
                   and self.map[node.i][node.j - 1] != MOUNTAIN

        elif direction == DIRECTION_RIGHT:
            return node.j + 1 < self.dimension \
                   and self.map[node.i][node.j + 1] != MOUNTAIN

        elif direction == DIRECTION_UP_LEFT:
            return (node.i - 1) >= 0 and (node.j - 1 >= 0) \
                   and not self.map[node.i - 1][node.j - 1] == MOUNTAIN

        elif direction == DIRECTION_UP_RIGHT:
            return (node.i - 1) >= 0 and (node.j + 1 < self.dimension) \
                   and not self.map[node.i - 1][node.j + 1] == MOUNTAIN

        elif direction == DIRECTION_DOWN_LEFT:
            return (node.i + 1) < self.dimension and (node.j - 1 >= 0) \
                   and not self.map[node.i + 1][node.j - 1] == MOUNTAIN

        elif direction == DIRECTION_DOWN_RIGHT:
            return (node.i + 1) < self.dimension and (node.j + 1 < self.dimension) \
                   and not self.map[node.i + 1][node.j + 1] == MOUNTAIN
        else:
            raise ValueError("No such a direction")

    def _can_move_to(self, close_nodes, node, direction):
        if direction == DIRECTION_UP:
            return node.i - 1 >= 0 \
                   and not self.map[node.i - 1][node.j] == MOUNTAIN \
                   and not close_nodes.__contains__((node.i - 1, node.j))

        elif direction == DIRECTION_DOWN:
            return node.i + 1 < self.dimension \
                   and not self.map[node.i + 1][node.j] == MOUNTAIN \
                   and not close_nodes.__contains__((node.i + 1, node.j))

        elif direction == DIRECTION_LEFT:
            return node.j - 1 >= 0 \
                   and self.map[node.i][node.j - 1] != MOUNTAIN \
                   and not close_nodes.__contains__((node.i, node.j - 1))

        elif direction == DIRECTION_RIGHT:
            return node.j + 1 < self.dimension \
                   and self.map[node.i][node.j + 1] != MOUNTAIN \
                   and not close_nodes.__contains__((node.i, node.j + 1))

        elif direction == DIRECTION_UP_LEFT:
            return (node.i - 1) >= 0 and (node.j - 1 >= 0) \
                   and not self.map[node.i - 1][node.j - 1] == MOUNTAIN \
                   and not close_nodes.__contains__((node.i - 1, node.j - 1))

        elif direction == DIRECTION_UP_RIGHT:
            return (node.i - 1) >= 0 and (node.j + 1 < self.dimension) \
                   and not self.map[node.i - 1][node.j + 1] == MOUNTAIN \
                   and not close_nodes.__contains__((node.i - 1, node.j + 1))

        elif direction == DIRECTION_DOWN_LEFT:
            return (node.i + 1) < self.dimension and (node.j - 1 >= 0) \
                   and not self.map[node.i + 1][node.j - 1] == MOUNTAIN \
                   and not close_nodes.__contains__((node.i + 1, node.j - 1))

        elif direction == DIRECTION_DOWN_RIGHT:
            return (node.i + 1) < self.dimension and (node.j + 1 < self.dimension) \
                   and not self.map[node.i + 1][node.j + 1] == MOUNTAIN \
                   and not close_nodes.__contains__((node.i + 1, node.j + 1))
        else:
            raise ValueError("No such a direction")

    def find_solution(self):
        """
        Call this method to obtain the solution
        :return: the path from start node to goal node and the total cost of the path
        """
        return [], -1

    def _find_solution_rec(self, current_map, current_node, explored_position):
        """
        Call by self.find_solution to explore the path recursively
        :param current_map:
        :param current_node:
        :param explored_position:
        :param open_nodes:
        :param close_nodes:
        :return: the path from the current node to the goal node and the cost of the sub-path
        """
        return [], -1
