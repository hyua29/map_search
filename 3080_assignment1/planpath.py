import bisect
import copy

COST_DIAGONAL_MOVE = 1
COST_NORMAL_MOVE = 2

NORMAL_PATH = "M"
MOUNTAIN = "X"
START_POINT = "S"
END_POINT = "G"

DIRECTION_UP = "U"
DIRECTION_DOWN = "D"
DIRECTION_LEFT = "L"
DIRECTION_RIGHT = "R"
DIRECTION_UP_LEFT = "UL"
DIRECTION_UP_RIGHT = "UR"
DIRECTION_DOWN_LEFT = "DL"
DIRECTION_DOWN_RIGHT = "DR"


class Node:

    def __init__(self, identifier=None, i=-1, j=-1, g=0, h=0, path_to_node=None, step_to_node=None, is_goal=False):
        self.identifier = identifier
        self.i = i
        self.j = j
        self.g = g
        self.h = h
        self.f = g + h
        self.path_to_node = path_to_node
        self.step_to_node = step_to_node
        self.is_goal = is_goal

    def move(self, direction, action_cost, new_h=0):
        """
        make a move
        :param direction: direction to move on
        :param action_cost: the cost of the move
        :param new_h: new heuristic cost of this node
        :return: new node
        """
        path_copy = copy.deepcopy(self.path_to_node)
        path_copy.append(direction)
        if direction == DIRECTION_UP:
            return Node(i=self.i-1, j=self.j, g=self.g+action_cost, h=new_h,
                        path_to_node=path_copy, step_to_node=direction)
        elif direction == DIRECTION_DOWN:
            return Node(i=self.i+1, j=self.j, g=self.g + action_cost, h=new_h,
                        path_to_node=path_copy, step_to_node=direction)
        elif direction == DIRECTION_LEFT:
            return Node(i=self.i, j=self.j-1, g=self.g + action_cost, h=new_h,
                        path_to_node=path_copy, step_to_node=direction)
        elif direction == DIRECTION_RIGHT:
            return Node(i=self.i1, j=self.j+1, g=self.g + action_cost, h=new_h,
                        path_to_node=path_copy, step_to_node=direction)
        elif direction == DIRECTION_UP_LEFT:
            return Node(i=self.i-1, j=self.j-1, g=self.g + action_cost, h=new_h,
                        path_to_node=path_copy, step_to_node=direction)
        elif direction == DIRECTION_UP_RIGHT:
            return Node(i=self.i-1, j=self.j+1, g=self.g + action_cost, h=new_h,
                        path_to_node=path_copy, step_to_node=direction)
        elif direction == DIRECTION_DOWN_LEFT:
            return Node(i=self.i+1, j=self.j-1, g=self.g + action_cost, h=new_h,
                        path_to_node=path_copy, step_to_node=direction)
        elif direction == DIRECTION_DOWN_RIGHT:
            return Node(i=self.i+1, j=self.j+1, g=self.g + action_cost, h=new_h,
                        path_to_node=path_copy, step_to_node=direction)

    def __eq__(self, other):
        return self.f == other.f

    def __lt__(self, other):
        return self.f < other.f

    def __gt__(self, other):
        return self.f > other.f

    def __ne__(self, other):
        return self.f != other.f

    def __le__(self, other):
        return self.f <= other.f

    def __ge__(self, other):
        return self.f >= other.f


def read_from_file(file_name):
    """
    parse the INPUT file
    :param file_name:
    :return: dimension: the dimension of the INPUT map; question_map: an 2D array representative of the map
    """
    # You can change the file reading function to suit the way
    # you want to parse the file
    file_handle = open(file_name)
    dimension = int(file_handle.readline())
    question_map = [["" for _ in range(dimension)] for _ in range(dimension)]
    for i in range(dimension):
        row = file_handle.readline()
        for j in range(dimension):
            question_map[i][j] = row[j]

    return dimension, question_map


def make_a_move(current_map, c_i, c_j, t_i, t_j):
    """
    make a move
    swap the symbol of current location with the target location
    :param current_map:
    :param c_i: current i
    :param c_j: current j
    :param t_i: target i
    :param t_j: target_j
    :return True if the target location is the goal location otherwise False
    """
    has_reached_goal = current_map[t_i][t_j] == END_POINT
    current_char = current_map[c_i][c_j]
    current_map[c_i][c_j] = current_map[t_i][t_j]
    current_map[t_i][t_j] = current_char

    return has_reached_goal


def DLS(start_map, map_dimension, max_depth):
    """ find the starting point """
    s_i = -1
    s_j = -1
    for i in range(map_dimension):
        for j in range(map_dimension):
            if start_map[i][j] == START_POINT:
                s_i = i
                s_j = j

    """ find the ending point """
    g_i = -1
    g_j = -1
    for i in range(map_dimension):
        for j in range(map_dimension):
            if start_map[i][j] == END_POINT:
                g_i = i
                g_j = j

    empty_path = []
    current_cost = 0
    explored_position = []
    start_node = Node(identifier="N0", i=s_i, j=s_j, g=0, h=0, path_to_node=["S"], step_to_node="S")
    """ (node, path, next_direction, g, h, f) """
    open_nodes = [start_node]
    result_path, total_cost = rec_DLS(start_map, map_dimension, max_depth,
                                      empty_path, current_cost, explored_position, open_nodes, close_nodes,
                                      s_i, s_j, g_i, g_j, has_reached_goal=False)
    return result_path, total_cost


def rec_DLS(current_map, map_dimension, max_depth,
            path_to_current_position, current_cost, explored_position, open_nodes, close_nodes,
            c_i, c_j, g_i, g_j, has_reached_goal=False):

    """ pick the node on the leftmost in order to perform depth limited search """
    node = open_nodes[0]
    """ remove from the open and put it in close """
    close_nodes.append(node)
    open_nodes.remove(node)
    original_open_len = len(open_nodes)
    child_number = 0

    """ expand the node """
    """ can go UL """
    if (node.i-1) >= 0 and (node.j-1 >= 0) \
            and not current_map[node.i-1][node.j-1] == MOUNTAIN \
            and not explored_position.__contains__((node.i-1, node.j-1)):
        new_node = node.move(DIRECTION_UP_LEFT, COST_DIAGONAL_MOVE, 0)
        map_copy = copy.deepcopy(current_map)
        has_reached_goal = make_a_move(map_copy, node.i, node.j, node.i - 1, node.j - 1)  # move towards up by one
        new_node.is_goal = has_reached_goal
        open_nodes.insert(0, new_node)
        child_number += 1
        # result_path, total_cost = rec_DLS(map_copy, map_dimension, max_depth,
        #                                   updated_path, new_cost, explored_position, open_nodes,
        #                                   c_i - 1, c_j - 1, g_i, g_j, has_reached_goal=has_reached_goal)

    """ can go UR """
    if (node.i - 1) >= 0 and (node.j + 1 < map_dimension) \
            and not current_map[node.i - 1][node.j + 1] == MOUNTAIN \
            and not explored_position.__contains__((node.i - 1, node.j + 1)):
        new_node = node.move(DIRECTION_UP_RIGHT, COST_DIAGONAL_MOVE, 0)
        map_copy = copy.deepcopy(current_map)
        has_reached_goal = make_a_move(map_copy, node.i, node.j, node.i - 1, node.j + 1)  # move towards up by one
        new_node.is_goal = has_reached_goal
        open_nodes.append(new_node)
        child_number += 1

    """ can go DL """
    if (node.i + 1) < map_dimension and (node.j - 1 >= 0) \
            and not current_map[node.i + 1][node.j - 1] == MOUNTAIN \
            and not explored_position.__contains__((node.i + 1, node.j - 1)):
        new_node = node.move(DIRECTION_DOWN_LEFT, COST_DIAGONAL_MOVE, 0)
        map_copy = copy.deepcopy(current_map)
        has_reached_goal = make_a_move(map_copy, node.i, node.j, node.i + 1, node.j - 1)  # move towards up by one
        new_node.is_goal = has_reached_goal
        open_nodes.insert(0, new_node)
        child_number += 1

    """ can go DR """
    if (node.i + 1) < map_dimension and (node.j + 1 < map_dimension) \
            and not current_map[node.i + 1][node.j + 1] == MOUNTAIN \
            and not explored_position.__contains__((node.i + 1, node.j + 1)):
        new_node = node.move(DIRECTION_DOWN_RIGHT, COST_DIAGONAL_MOVE, 0)
        map_copy = copy.deepcopy(current_map)
        has_reached_goal = make_a_move(map_copy, node.i, node.j, node.i + 1, node.j + 1)  # move towards up by one
        new_node.is_goal = has_reached_goal
        open_nodes.insert(0, new_node)
        child_number += 1


    """ can go up """
    if node.i - 1 >= 0 \
            and not current_map[node.i - 1][node.j] == MOUNTAIN \
            and not explored_position.__contains__((node.i - 1, node.j)):
        new_node = node.move(DIRECTION_UP, COST_NORMAL_MOVE, 0)
        map_copy = copy.deepcopy(current_map)
        has_reached_goal = make_a_move(map_copy, node.i, node.j, node.i - 1, node.j)  # move towards up by one
        new_node.is_goal = has_reached_goal
        open_nodes.insert(0, new_node)
        child_number += 1
    """ can go down """
    if node.i + 1 < map_dimension \
            and current_map[node.i + 1][node.j] != MOUNTAIN \
            and not explored_position.__contains__((node.i + 1, node.j)):
        new_node = node.move(DIRECTION_DOWN, COST_NORMAL_MOVE, 0)
        map_copy = copy.deepcopy(current_map)
        has_reached_goal = make_a_move(map_copy, node.i, node.j, node.i + 1, node.j)  # move towards up by one
        new_node.is_goal = has_reached_goal
        open_nodes.insert(0, new_node)
        child_number += 1

    """ can go left """
    if node.j - 1 >= 0 \
            and current_map[node.i][node.j - 1] != MOUNTAIN\
            and not explored_position.__contains__((node.i, node.j - 1)):
        new_node = node.move(DIRECTION_LEFT, COST_NORMAL_MOVE, 0)
        map_copy = copy.deepcopy(current_map)
        has_reached_goal = make_a_move(map_copy, node.i, node.j, node.i, node.j-1)  # move towards up by one
        new_node.is_goal = has_reached_goal
        open_nodes.insert(0, new_node)
        child_number += 1

    """ can go right """
    if node.j + 1 < map_dimension\
            and current_map[node.i][node.j + 1] != MOUNTAIN\
            and not explored_position.__contains__((node.i, node.j + 1)):
        new_node = node.move(DIRECTION_LEFT, COST_NORMAL_MOVE, 0)
        map_copy = copy.deepcopy(current_map)
        has_reached_goal = make_a_move(map_copy, node.i, node.j, node.i, node.j+1)  # move towards up by one
        new_node.is_goal = has_reached_goal
        open_nodes.insert(0, new_node)
        child_number += 1

    """ no solution on this path """
    is_dead_end = len(open_nodes) > original_open_len
    if is_dead_end:
        return None

    for i in range(child_number):
        result_path, total_cost = rec_DLS(map_copy, map_dimension, max_depth,
                                      updated_path, new_cost, explored_position, open_nodes,
                                      c_i - 1, c_j - 1, g_i, g_j, has_reached_goal=has_reached_goal)


    """ go to each node"""


    result_path = []
    total_cost = -1

    """ record current position """
    explored_position.append((c_i, c_j))

    """ has reached max depth"""
    if len(path_to_current_position) > max_depth:
        print("Exceeded the max depth")
        return [], -1
    """ return the result if the goal has been found """
    if has_reached_goal:
        return path_to_current_position, current_cost

    # """ can go UL """
    # if (c_i-1) >= 0 and (c_j-1 >= 0)\
    #         and not current_map[c_i - 1][c_j-1] == MOUNTAIN\
    #         and not explored_position.__contains__((c_i-1, c_j-1)):
    #     updated_path = copy.deepcopy(path_to_current_position)
    #     updated_path.append(DIRECTION_UP_LEFT)
    #     new_cost = current_cost + COST_DIAGONAL_MOVE
    #     map_copy = copy.deepcopy(current_map)
    #     has_reached_goal = make_a_move(map_copy, c_i, c_j, c_i-1, c_j-1)  # move towards up by one
    #
    #     result_path, total_cost = rec_DLS(map_copy, map_dimension, max_depth,
    #                                       updated_path, new_cost, explored_position, open_nodes,
    #                                       c_i - 1, c_j-1, g_i, g_j, has_reached_goal=has_reached_goal)
    #
    #
    # """ can go UR """
    # if (c_i-1) >= 0 and (c_j+1 < map_dimension)\
    #         and not current_map[c_i - 1][c_j+1] == MOUNTAIN\
    #         and not explored_position.__contains__((c_i-1, c_j+1)):
    #     updated_path = copy.deepcopy(path_to_current_position)
    #     updated_path.append(DIRECTION_UP_RIGHT)
    #     new_cost = COST_DIAGONAL_MOVE + current_cost
    #     map_copy = copy.deepcopy(current_map)
    #     has_reached_goal = make_a_move(map_copy, c_i, c_j, c_i-1, c_j+1)  # move towards up by one
    #
    #     result_path, total_cost = rec_DLS(map_copy, map_dimension, max_depth,
    #                                       updated_path, new_cost, explored_position, open_nodes,
    #                                       c_i-1, c_j+1, g_i, g_j, has_reached_goal=has_reached_goal)
    #
    # """ can go DL """
    # if (c_i+1) < map_dimension and (c_j-1 >= 0)\
    #         and not current_map[c_i+1][c_j-1] == MOUNTAIN\
    #         and not explored_position.__contains__((c_i+1, c_j-1)):
    #     updated_path = copy.deepcopy(path_to_current_position)
    #     updated_path.append(DIRECTION_DOWN_LEFT)
    #     new_cost = COST_DIAGONAL_MOVE + current_cost
    #     map_copy = copy.deepcopy(current_map)
    #     has_reached_goal = make_a_move(map_copy, c_i, c_j, c_i+1, c_j-1)  # move towards up by one
    #
    #     result_path, total_cost = rec_DLS(map_copy, map_dimension, max_depth,
    #                                       updated_path, new_cost, explored_position, open_nodes,
    #                                       c_i+1, c_j-1, g_i, g_j, has_reached_goal=has_reached_goal)
    #
    # """ can go DR """
    # if (c_i+1) < map_dimension and (c_j+1 < map_dimension)\
    #         and not current_map[c_i+1][c_j+1] == MOUNTAIN\
    #         and not explored_position.__contains__((c_i+1, c_j+1)):
    #     updated_path = copy.deepcopy(path_to_current_position)
    #     updated_path.append(DIRECTION_DOWN_RIGHT)
    #     new_cost = COST_DIAGONAL_MOVE + current_cost
    #     map_copy = copy.deepcopy(current_map)
    #     has_reached_goal = make_a_move(map_copy, c_i, c_j, c_i+1, c_j+1)  # move towards up by one
    #
    #     result_path, total_cost = rec_DLS(map_copy, map_dimension, max_depth,
    #                                       updated_path, new_cost, explored_position, open_nodes,
    #                                       c_i+1, c_j+1, g_i, g_j, has_reached_goal=has_reached_goal)
    #
    # """ can go up """
    # if (c_i - 1) >= 0\
    #         and not current_map[c_i - 1][c_j] == MOUNTAIN\
    #         and not explored_position.__contains__((c_i - 1, c_j)):
    #     updated_path = copy.deepcopy(path_to_current_position)
    #     updated_path.append(DIRECTION_UP)
    #     new_cost = COST_NORMAL_MOVE + current_cost
    #     map_copy = copy.deepcopy(current_map)
    #     has_reached_goal = make_a_move(map_copy, c_i, c_j, c_i - 1, c_j)  # move towards up by one
    #
    #     result_path, total_cost = rec_DLS(map_copy, map_dimension, max_depth,
    #                                       updated_path, new_cost, explored_position, open_nodes,
    #                                       c_i - 1, c_j, g_i, g_j, has_reached_goal=has_reached_goal)
    #
    # """ can go down """
    # if (c_i + 1) < map_dimension and (current_map[c_i + 1][c_j] != MOUNTAIN) and not explored_position.__contains__(
    #         (c_i + 1, c_j)):
    #     updated_path = copy.deepcopy(path_to_current_position)
    #     updated_path.append(DIRECTION_DOWN)
    #     new_cost = COST_NORMAL_MOVE + current_cost
    #     map_copy = copy.deepcopy(current_map)
    #     has_reached_goal = make_a_move(map_copy, c_i, c_j, c_i + 1, c_j)  # move towards down by one
    #
    #     result_path, total_cost = rec_DLS(map_copy, map_dimension, max_depth,
    #                                       updated_path, new_cost, explored_position, open_nodes,
    #                                       c_i + 1, c_j, g_i, g_j, has_reached_goal=has_reached_goal)
    #
    # """ can go left """
    # if (c_j - 1) >= 0 and (current_map[c_i][c_j - 1] != MOUNTAIN) and not explored_position.__contains__(
    #         (c_i, c_j - 1)):
    #     updated_path = copy.deepcopy(path_to_current_position)
    #     updated_path.append(DIRECTION_LEFT)
    #     new_cost = COST_NORMAL_MOVE + current_cost
    #     map_copy = copy.deepcopy(current_map)
    #     has_reached_goal = make_a_move(map_copy, c_i, c_j, c_i, c_j - 1)  # move towards left by one
    #
    #     result_path, total_cost = rec_DLS(map_copy, map_dimension, max_depth,
    #                                       updated_path, new_cost, explored_position, open_nodes,
    #                                       c_i, c_j - 1, g_i, g_j, has_reached_goal=has_reached_goal)
    #
    # """ can go right """
    # if (c_j + 1) < map_dimension and (current_map[c_i][c_j + 1] != MOUNTAIN) and not explored_position.__contains__(
    #         (c_i, c_j + 1)):
    #     updated_path = copy.deepcopy(path_to_current_position)
    #     updated_path.append(DIRECTION_RIGHT)
    #     new_cost = COST_NORMAL_MOVE + current_cost
    #     map_copy = copy.deepcopy(current_map)
    #     has_reached_goal = make_a_move(map_copy, c_i, c_j, c_i, c_j + 1)  # move towards right by one
    #
    #     result_path, total_cost = rec_DLS(map_copy, map_dimension, max_depth,
    #                                       updated_path, new_cost, explored_position, open_nodes,
    #                                       c_i, c_j + 1, g_i, g_j, has_reached_goal=has_reached_goal)

    if not total_cost == -1:
        return result_path, total_cost

    """ dead end """
    return [], -1

#
# map_d, start_map = read_from_file("sample_input.txt")
# path, cost = DLS(start_map, map_d, 200)
#
# print(path)
# print(cost)


a = Node(g=1, h=-1)
b = Node(g=10, h=0)
list = [a, b]
bisect.insort_left(list, Node(g=4))
c = Node(g=1)

for node in list:
    print(node.g)