import bisect
from search_helper import Node
from search_helper import Search
import search_helper


class ASearch(Search):

    def _get_heuristic_cost(self, current_node):
        """
        Calculating the distance from the current position to the goal
        without considering the mountain. If the current node and the goal node are in the same row or column,
        the cost of two consecutive horizontal or perpendicular moves will be replaced by two diagonal moves.
        Since there might be mountain nodes standing in the way, the actual path towards the goal must be greater than
        or equal to the heuristic cost, and hence the heuristic function is guaranteed to be admissible.
        :param current_node: the node from which the cost is calculated
        :return: h
        """
        c_i = current_node.i
        c_j = current_node.j
        h_cost = 0  # the heuristic cost of the current node

        """ should move diagonally """
        while not c_i == self.goal_node_position[0] and not c_j == self.goal_node_position[1]:
            if c_i < self.goal_node_position[0] and c_j < self.goal_node_position[1]:  # should move to bottom right
                c_i += 1
                c_j += 1
            if c_i < self.goal_node_position[0] and c_j > self.goal_node_position[1]:  # should move to bottom left
                c_i += 1
                c_j -= 1
            if c_i > self.goal_node_position[0] and c_j < self.goal_node_position[1]:  # should move to top right
                c_i -= 1
                c_j += 1
            if c_i > self.goal_node_position[0] and c_j > self.goal_node_position[1]:  # should move to top left
                c_i -= 1
                c_j -= 1

            h_cost += search_helper.COST_DIAGONAL_MOVE

        """ should move perpendicularly """
        i_difference = abs(c_i - self.goal_node_position[0])
        while not i_difference == 0:
            if i_difference >= 2:  # two perpendicular moves can be replaced by two diagonal moves
                h_cost += search_helper.COST_DIAGONAL_MOVE * 2
                i_difference -= 2
            elif i_difference == 1:
                h_cost += search_helper.COST_NORMAL_MOVE
                i_difference -= 1
            else:
                raise ValueError("The i index must be an integer")

        """ should move horizontally """
        j_difference = abs(c_j - self.goal_node_position[1])
        while not j_difference == 0:
            if j_difference >= 2:  # two perpendicular moves can be replaced by two diagonal moves
                h_cost += search_helper.COST_DIAGONAL_MOVE * 2
                j_difference -= 2
            elif j_difference == 1:
                h_cost += search_helper.COST_NORMAL_MOVE
                j_difference -= 1
            else:
                raise ValueError("The j index must be an integer")

        return h_cost

    def find_solution(self):
        """ find the starting point """
        s_i = -1
        s_j = -1
        for i in range(self.dimension):
            for j in range(self.dimension):
                if self.map[i][j] == search_helper.START_POINT:
                    s_i = i
                    s_j = j
                if self.map[i][j] == search_helper.END_POINT:
                    self.goal_node_position = (i, j)

        """ create the starting point of the map """
        explored_position = []
        # TODO: get the h cost for the first node; for now, it's set to 1
        start_node = Node(self._get_id_counter(), i=s_i, j=s_j, g=0, h=1, path_to_node=["S"], step_to_node="S",
                          expansion_order=self._get_expansion_order())
        self.open = [start_node]
        self.close = []
        return self._find_solution_rec(self.map, start_node, explored_position)

    def _find_solution_rec(self, current_map, current_node, explored_position):

        """ remove from the open and put it in close """
        self.close.append(current_node)
        self.open.remove(current_node)
        # TODO: replace this with close
        explored_position.append((current_node.i, current_node.j))

        """ return the result if the goal has been found """
        if current_node.is_goal:
            """ print diagnostic message """
            if self.diagnostic >= current_node.expansion_order:
                self._print_state(current_node)
            return current_node.path_to_node, current_node.f

        """ expand the node, straight moves """
        for key, direction in search_helper.straight_directions.items():
            if self._not_out_of_bounds(current_node, direction):
                new_node = current_node.move(direction, search_helper.COST_NORMAL_MOVE, new_h=0)

                if not (self.open.__contains__(new_node) or self.close.__contains__(
                        new_node)):  # if this node has never been found
                    new_node.set_identifier(self._get_id_counter())
                    new_node.update_h(self._get_heuristic_cost(new_node))
                    bisect.insort_left(self.open, new_node)
                elif self.open.__contains__(new_node):
                    index = self.open.index(new_node)
                    new_node = self.open[index]
                elif self.close.__contains__(new_node):
                    index = self.close.index(new_node)
                    new_node = self.close[index]

                current_node.add_child_id(new_node.identifier)

        """ diagonal moves """
        for key, direction in search_helper.diagonal_directions.items():
            if self._not_out_of_bounds(current_node, direction):
                new_node = current_node.move(direction, search_helper.COST_DIAGONAL_MOVE, new_h=0)

                if not (self.open.__contains__(new_node) or self.close.__contains__(
                        new_node)):  # if this node has never been found
                    new_node.set_identifier(self._get_id_counter())
                    new_node.update_h(self._get_heuristic_cost(new_node))
                    bisect.insort_left(self.open, new_node)
                elif self.open.__contains__(new_node):
                    index = self.open.index(new_node)
                    new_node = self.open[index]
                elif self.close.__contains__(new_node):
                    index = self.close.index(new_node)
                    new_node = self.close[index]

                current_node.add_child_id(new_node.identifier)

        """ print diagnostic message """
        if self.diagnostic >= current_node.expansion_order:
            self._print_state(current_node)

        """ expand the node that has the lowest f """
        self.open[0].expansion_order = self._get_expansion_order()
        modified_map = self.make_a_move(current_map, current_node, self.open[0])
        path_next_node, cost_next_node = self._find_solution_rec(modified_map, self.open[0], explored_position)

        """ found the solution"""
        if not cost_next_node == -1:
            return path_next_node, cost_next_node

        return [], -1


def read_from_file(file_name):
    """
    parse the INPUT file
    :param file_name:
    :return: dimension: the dimension of the INPUT map; question_map: an 2D array representative of the map
    """
    # You can change the file reading function to suit the way
    # you want to parse the file
    file_handle = open(file_name)
    dimension = int(file_handle.readline())
    print(dimension)
    question_map = [["" for _ in range(dimension)] for _ in range(dimension)]
    for i in range(dimension):
        row = file_handle.readline()
        for j in range(dimension):
            question_map[i][j] = row[j]

    return question_map, dimension


start_map, map_d = read_from_file("sample_input.txt")
a_search = ASearch(start_map, map_d, diagnostic=1000)
path, cost = a_search.find_solution()

print("The path that can reach the goal is: " + str(path))
print("Total cost of this path is: " + str(cost))

list = []
n1 = Node(identifier=-1)
n2 = Node(identifier=5)
list.append(n1)
print(list.__contains__(n2))
k = list.index(n2)
print(list[k])